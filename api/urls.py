from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import LoginView, LogoutView, SignupView


router = DefaultRouter()


urlpatterns = [
    path('auth/login/', LoginView.as_view(), name='auth-login'),
    path('auth/logout/', LogoutView.as_view(), name='auth-logout'),
    path('auth/signup/', SignupView.as_view(), name='auth-signup'),
] + router.urls

