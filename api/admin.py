from django.contrib import admin
from .models import User, Player, Team

admin.site.register(User)
@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'age', 'goals', 'assists', 'country', 'position', 'played_games')

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'position', 'played_matches', 'win_matches', 'draw_matches', 'defeat_matches', 'technical_manager', 'technical_assistant')