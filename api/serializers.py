from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import *
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password


# This is a serializer for the User model that requires a username, and a password that is at least 8
# characters long and is write-only.
class UserSerializer(ModelSerializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(
        min_length=8, write_only=True
    )

    def validate_password(self, value):
        """
        It takes a string as input, and returns a string that is the hashed version of the input

        :param value: The value of the field being validated
        :return: The value of the password is being hashed and salted.
        """
        return make_password(value)

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name',
                  'email', 'password')


class PlayerSerializer(ModelSerializer):
    class Meta:
        model = Player
        fields = ('name', 'born_date', 'age', 'goals', 'assists', 'country', 'position', 'played_games',
                  'titular', 'minutes', 'goals_without_penal', 'executed_penal', 'yellow_cards', 'red_cards',
                  'coverage_goals', 'coverage_assists', 'coverage_goals_assists', 'coverage_goals_without_penal',
                  'coverage_goals_assists_without_penal')


class TeamSerializer(ModelSerializer):
    class Meta:
        model = Team
        fields = ('name', 'city', 'position', 'played_matches', 'win_matches', 'draw_matches', 'defeat_matches',
                  'goals_scored', 'goals_against', 'goals_difference', 'points', 'points_per_match', 'people_assisted',
                  'goal_scorer', 'notes', 'technical_manager', 'technical_assistant')